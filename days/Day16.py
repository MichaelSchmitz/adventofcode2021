from __future__ import annotations
from typing import Tuple, List
import math

from .AbstractDay import AbstractDay


class BasePacket:
    def sum_of_packet_vers(self) -> int:
        return 0

    def perform_operation(self) -> int:
        return 0


class LiteralPacket(BasePacket):
    value: str
    version: int

    def perform_operation(self) -> int:
        return int(self.value, base=2)

    def __init__(self, packet_version: int):
        self.value = ''
        self.version = packet_version

    @classmethod
    def create_packet(cls, remaining_bits: str, packet_version: str) -> Tuple[LiteralPacket, str]:
        not_last = True
        position = 0
        packet = LiteralPacket(int(packet_version, base=2))
        while not_last:
            not_last = packet.read_number(remaining_bits[5 * position:5 * position + 5])
            position += 1
        return packet, remaining_bits[position * 5:]

    def read_number(self, five_bits: str) -> bool:
        self.value += five_bits[1:5]
        return five_bits[0] == '1'

    def sum_of_packet_vers(self) -> int:
        return self.version


class FixOperatorPacket(BasePacket):
    packets: []
    version: int
    operation = None

    @classmethod
    def create_packet(cls, remaining_bits: str, packet_version: str, operation: str) -> Tuple[FixOperatorPacket, str]:
        packet = FixOperatorPacket()
        packet.version = int(packet_version, base=2)
        packet.operation = get_operation_function(operation)
        sub_packets_length = int(remaining_bits[:15], base=2)
        packet_bits = remaining_bits[15:sub_packets_length + 15]
        packet.packets = read_packets(packet_bits)
        return packet, remaining_bits[sub_packets_length + 15:]

    def sum_of_packet_vers(self) -> int:
        return sum(packet.sum_of_packet_vers() for packet in self.packets) + self.version

    def perform_operation(self) -> int:
        return self.operation(*[packet.perform_operation() for packet in self.packets])


class CountOperatorPacket(BasePacket):
    packets: []
    version: int
    operation = None

    @classmethod
    def create_packet(cls, remaining_bits: str, packet_version: str, operation: str) -> Tuple[CountOperatorPacket, str]:
        packet = CountOperatorPacket()
        packet.version = int(packet_version, base=2)
        packet.operation = get_operation_function(operation)
        sub_packets = int(remaining_bits[:11], base=2)
        remaining_bits = remaining_bits[11:]
        packet.packets, remaining_bits = read_packets_counted(remaining_bits, sub_packets)
        return packet, remaining_bits

    def sum_of_packet_vers(self) -> int:
        return sum(packet.sum_of_packet_vers() for packet in self.packets) + self.version

    def perform_operation(self) -> int:
        return self.operation(*[packet.perform_operation() for packet in self.packets])


def get_operation_function(operation: str):
    if operation == '000':  # 0
        return lambda *x: sum(x)
    elif operation == '001':  # 1
        return lambda *x: math.prod(x)
    elif operation == '010':  # 2
        return lambda *x: min(x)
    elif operation == '011':  # 3
        return lambda *x: max(x)
    elif operation == '101':  # 5
        return lambda *x: 1 if x[0] > x[1] else 0
    elif operation == '110':  # 6
        return lambda *x: 1 if x[0] < x[1] else 0
    elif operation == '111':  # 7
        return lambda *x: 1 if x[0] == x[1] else 0


def read_packets_counted(binary: str, packet_count: int) -> Tuple[List[BasePacket], str]:
    packets = []
    for _ in range(packet_count):
        packet, binary = read_packet(binary)
        packets.append(packet)
    return packets, binary


def read_packet(binary: str) -> Tuple[BasePacket, str]:
    if len(binary) < 4:
        return BasePacket(), ''
    packet_version = binary[:3]
    packet_type = binary[3:6]
    if packet_type == '100':  # 4
        return LiteralPacket.create_packet(binary[6:], packet_version)
    else:
        if binary[6] == '0':
            return FixOperatorPacket.create_packet(binary[7:], packet_version, packet_type)
        else:
            return CountOperatorPacket.create_packet(binary[7:], packet_version, packet_type)


def read_packets(binary: str) -> List[BasePacket]:
    packets = []
    while len(binary) > 0 and int(binary, base=2) > 0:
        packet, binary = read_packet(binary)
        packets.append(packet)
    return packets


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        binary = self.get_binary()
        packets = read_packets(binary)
        return sum(packet.sum_of_packet_vers() for packet in packets)

    def process_input_part2(self) -> int:
        binary = self.get_binary()
        packet = read_packets(binary)
        return packet[0].perform_operation()

    def get_binary(self):
        binary = bin(int(self.puzzle_input[0], base=16))[2:]
        first_digit = int(self.puzzle_input[0][0], base=16)
        i = 0
        while first_digit == 0:
            if first_digit < 8:
                binary = '0' + binary
            if first_digit < 4:
                binary = '0' + binary
            if first_digit < 2:
                binary = '0' + binary
            if first_digit < 1:
                binary = '0' + binary
            i += 1
            first_digit = int(self.puzzle_input[0][i], base=16)
        if first_digit < 8:
            binary = '0' + binary
        if first_digit < 4:
            binary = '0' + binary
        if first_digit < 2:
            binary = '0' + binary
        if first_digit < 1:
            binary = '0' + binary
        return binary
