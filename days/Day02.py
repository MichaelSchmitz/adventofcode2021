from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        horizontal = 0
        depth = 0
        for step in self.puzzle_input:
            command, value = step.split(' ')
            if command == 'forward':
                horizontal += int(value)
            elif command == 'down':
                depth += int(value)
            elif command == 'up':
                depth -= int(value)
        return horizontal * depth

    def process_input_part2(self) -> int:
        horizontal = 0
        depth = 0
        aim = 0
        for step in self.puzzle_input:
            command, value = step.split(' ')
            if command == 'forward':
                horizontal += int(value)
                depth += int(value) * aim
            elif command == 'down':
                aim += int(value)
            elif command == 'up':
                aim -= int(value)
        return horizontal * depth
