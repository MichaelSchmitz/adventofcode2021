from __future__ import annotations
from typing import List, Dict, Tuple

from .AbstractDay import AbstractDay


def already_visited_small_twice(visited_caves: List[str], neighbour: str) -> bool:
    if neighbour in ['start', 'end']:
        return True
    for cave in visited_caves:
        if cave[0].islower() and visited_caves.count(cave) > 1:
            return True
    return False


class Cave:
    big: bool
    name: str
    neighbours: List[Cave]

    def __init__(self, cave_name: str) -> None:
        self.name = cave_name
        self.big = cave_name[0].isupper()
        self.neighbours = []

    def add_neighbour(self, neighbour: Cave) -> None:
        self.neighbours.append(neighbour)

    def neighbour_routes(self, current_route: str) -> List[Tuple[str, Cave]]:
        result = []
        for neighbour in self.neighbours:
            if neighbour.big or neighbour.name not in current_route:
                result.append((current_route + ',' + neighbour.name, neighbour))
        return result

    def neighbour_routes_with_one_small_twice(self, current_route: str) -> List[Tuple[str, Cave]]:
        result = []
        visited_caves = current_route.split(',')
        for neighbour in self.neighbours:
            if neighbour.big or neighbour.name not in current_route or not already_visited_small_twice(visited_caves,
                                                                                                       neighbour.name):
                result.append((current_route + ',' + neighbour.name, neighbour))
        return result


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        start = self.generate_cave_net()
        routes = []
        start.visited = True
        unfinished_routes = start.neighbour_routes('start')
        while len(unfinished_routes) > 0:
            for route, cave in unfinished_routes[:]:
                if cave.name == 'end':
                    routes.append(route)
                    unfinished_routes.remove((route, cave))
                else:
                    new_routes = cave.neighbour_routes(route)
                    for new_route in new_routes:
                        unfinished_routes.append(new_route)
                    unfinished_routes.remove((route, cave))
        return len(routes)

    def process_input_part2(self) -> int:
        start = self.generate_cave_net()
        routes = []
        start.visited = True
        unfinished_routes = start.neighbour_routes_with_one_small_twice('start')
        while len(unfinished_routes) > 0:
            for route, cave in unfinished_routes[:]:
                if cave.name == 'end':
                    routes.append(route)
                    unfinished_routes.remove((route, cave))
                else:
                    new_routes = cave.neighbour_routes_with_one_small_twice(route)
                    for new_route in new_routes:
                        unfinished_routes.append(new_route)
                    unfinished_routes.remove((route, cave))
        return len(routes)

    def generate_cave_net(self) -> Cave:
        caves = {}
        for pair in self.puzzle_input:
            left, right = pair.split('-')
            if left not in caves:
                caves[left] = Cave(left)
            if right not in caves:
                caves[right] = Cave(right)
            caves[left].add_neighbour(caves[right])
            caves[right].add_neighbour(caves[left])
        return caves['start']
