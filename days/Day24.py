from typing import List

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        max_num = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        stack = []
        for num_index, block in enumerate(self.get_num_blocks()):
            div_instruction = block[3]
            if div_instruction == 'div z 1':  # add fix number to stack on index
                fix_num_add = int(block[14].split()[-1])
                stack.append((num_index, fix_num_add))
            elif div_instruction == 'div z 26':  # calc diff for latest stack and new fix num
                _num_index, fix_num_add = stack.pop()
                sub_diff = fix_num_add + int(block[4].split()[-1])
                if sub_diff < 0:  # invert
                    num_index, _num_index, sub_diff = _num_index, num_index, -sub_diff
                # update nums with new diff
                max_num[num_index] = 9
                max_num[_num_index] = 9 - sub_diff
        return int(''.join(map(str, max_num)))

    def get_num_blocks(self) -> List[List[str]]:
        blocks = []
        block = []
        for instruction in self.puzzle_input:
            if instruction == 'inp w':
                if len(block) != 0:
                    blocks.append(block)
                block = []
            else:
                block.append(instruction)
        blocks.append(block)
        return blocks

    def process_input_part2(self) -> int:
        min_num = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        stack = []
        for num_index, block in enumerate(self.get_num_blocks()):
            div_instruction = block[3]
            if div_instruction == 'div z 1':  # add fix number to stack on index
                fix_num_add = int(block[14].split()[-1])
                stack.append((num_index, fix_num_add))
            elif div_instruction == 'div z 26':  # calc diff for latest stack and new fix num
                _num_index, fix_num_add = stack.pop()
                sub_diff = fix_num_add + int(block[4].split()[-1])
                if sub_diff < 0:  # invert
                    num_index, _num_index, sub_diff = _num_index, num_index, -sub_diff
                # update nums with new diff
                min_num[num_index] = 1 + sub_diff
                min_num[_num_index] = 1
        return int(''.join(map(str, min_num)))
