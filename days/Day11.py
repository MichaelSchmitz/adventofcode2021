from __future__ import annotations

from typing import List, Dict

from .AbstractDay import AbstractDay


class Coord:
    def __init__(self, x: int, y: int) -> None:
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other) -> bool:
        if isinstance(other, Coord):
            return self.x == other.x and self.y == other.y
        return False

    def __hash__(self):
        return hash((self.x, self.y))

    def get_neighbour_coords(self) -> List[Coord]:
        coords = []
        for x in range(-1, 2):
            for y in range(-1, 2):
                if x == 0 and y == 0:
                    continue
                coords.append(Coord(self.x + x, self.y + y))
        return coords


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        octopuses = {}
        for y, row in enumerate(self.puzzle_input):
            for x, num in enumerate(row):
                octopuses[Coord(x, y)] = int(num)

        flashes = 0
        for _ in range(100):
            for coord in octopuses.keys():
                octopuses[coord] += 1
            for coord, octopus in octopuses.items():
                flashes += self.process_flashes(coord, octopus, octopuses)
        return flashes

    def process_flashes(self, coord: Coord, octopus: int, octopuses: Dict[Coord, int]) -> int:
        flashes = 0
        if octopus > 9:
            octopuses[coord] = 0
            flashes += 1
            neighbours = coord.get_neighbour_coords()
            for neighbour in neighbours:
                if neighbour in octopuses.keys():
                    if octopuses[neighbour] > 0:
                        octopuses[neighbour] += 1
                    flashes += self.process_flashes(neighbour, octopuses[neighbour], octopuses)
        return flashes

    def process_input_part2(self) -> int:
        octopuses = {}
        for y, row in enumerate(self.puzzle_input):
            for x, num in enumerate(row):
                octopuses[Coord(x, y)] = int(num)

        flashes = 0
        iterations = 0
        while flashes != 100:
            iterations += 1
            flashes = 0
            for coord in octopuses.keys():
                octopuses[coord] += 1
            for coord, octopus in octopuses.items():
                flashes += self.process_flashes(coord, octopus, octopuses)
        return iterations
