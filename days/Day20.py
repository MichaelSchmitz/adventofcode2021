from typing import Tuple, List
from .AbstractDay import AbstractDay


def enhance(algo: List[bool], pixels: List[Tuple[int, int]],
            iteration: int) -> List[Tuple[int, int]]:
    min_x = min(x for x, _ in pixels)
    max_x = max(x for x, _ in pixels)
    min_y = min(y for _, y in pixels)
    max_y = max(y for _, y in pixels)

    def enhance_pixel(x: int, y: int) -> bool:

        def is_lighted(_x, _y):
            if algo[0] and not (min_x <= _x <= max_x and min_y <= _y <= max_y):
                return iteration % 2
            return (_x, _y) in pixels

        index = 0
        for ny in range(y - 1, y + 2):
            for nx in range(x - 1, x + 2):
                index = index << 1 | is_lighted(nx, ny)
        return algo[index]

    return [(x, y) for y in range(min_y - 1, max_y + 2) for x in range(min_x - 1, max_x + 2) if enhance_pixel(x, y)]


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        return self.enhance_image(2)

    def enhance_image(self, iterations: int) -> int:
        algo = [val == '#' for val in self.puzzle_input[0]]
        lit_pixels = []
        for y, row in enumerate(self.puzzle_input[2:]):
            for x, pixel in enumerate(row):
                if pixel == '#':
                    lit_pixels.append((x, y))
        for i in range(iterations):
            lit_pixels = enhance(algo, lit_pixels, i)
        return len(lit_pixels)

    def process_input_part2(self) -> int:
        return self.enhance_image(50)
