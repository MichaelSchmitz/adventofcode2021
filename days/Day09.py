from typing import Tuple, Dict, List

from .AbstractDay import AbstractDay


def is_low_point(pos: Tuple[int, int], depth: int, depths: Dict[Tuple[int, int], Tuple[int, bool]]) -> bool:
    left, right, top, down = get_neighbours(pos)
    if left in depths and depths[left][0] <= depth:
        return False
    if right in depths and depths[right][0] <= depth:
        return False
    if top in depths and depths[top][0] <= depth:
        return False
    if down in depths and depths[down][0] <= depth:
        return False
    return True


def find_all_neighbours(low_point: Tuple[int, int], depths: Dict[Tuple[int, int], Tuple[int, bool]]) -> int:
    left, right, top, down = get_neighbours(low_point)
    count = 0
    if left in depths and depths[left][0] != 9 and not depths[left][1]:
        depths[left] = depths[left][0], True
        count += find_all_neighbours(left, depths) + 1
    if right in depths and depths[right][0] != 9 and not depths[right][1]:
        depths[right] = depths[right][0], True
        count += find_all_neighbours(right, depths) + 1
    if top in depths and depths[top][0] != 9 and not depths[top][1]:
        depths[top] = depths[top][0], True
        count += find_all_neighbours(top, depths) + 1
    if down in depths and depths[down][0] != 9 and not depths[down][1]:
        depths[down] = depths[down][0], True
        count += find_all_neighbours(down, depths) + 1
    return count


def get_neighbours(pos: Tuple[int, int]) -> Tuple[Tuple[int, int], Tuple[int, int], Tuple[int, int], Tuple[int, int]]:
    left = (pos[0] - 1, pos[1])
    right = (pos[0] + 1, pos[1])
    top = (pos[0], pos[1] - 1)
    down = (pos[0], pos[1] + 1)
    return left, right, top, down


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        depths = self.generate_depth_map()
        risk_levels = []
        for pos, depth in depths.items():
            if is_low_point(pos, depth[0], depths):
                risk_levels.append(depth[0] + 1)
        return sum(risk_levels)

    def process_input_part2(self) -> int:
        depths = self.generate_depth_map()
        low_points = {}
        for pos, depth in depths.items():
            if is_low_point(pos, depth[0], depths):
                low_points[pos] = False

        basins = []
        for low_point, already_visited in low_points.items():
            if already_visited:
                continue
            else:
                basins.append(find_all_neighbours(low_point, depths))

        result = 1
        for basin in sorted(basins)[-3:]:
            result *= basin
        return result

    def generate_depth_map(self) -> Dict[Tuple[int, int], Tuple[int, bool]]:
        depths = {}
        for y, row in enumerate(self.puzzle_input):
            for x, point in enumerate(row):
                depths[(x, y)] = int(point), False
        return depths
