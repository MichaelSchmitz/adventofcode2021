from typing import Tuple, List, Set, Union

from .AbstractDay import AbstractDay


def reorient_all(beacons: Set[Tuple[int, int, int]], scanners: List[Tuple[int, int, int]],
                 non_matched: List[List[Tuple[int, int, int]]]) -> None:
    while non_matched:
        for readings in non_matched:
            scanner_beacons, scanner_position = orient_align(beacons, readings)
            if scanner_beacons:
                non_matched.remove(readings)
                beacons |= scanner_beacons
                scanners.append(scanner_position)


def orient_align(beacons: Set[Tuple[int, int, int]], readings: List[Tuple[int, int, int]]) -> \
        Tuple[Union[None, Set[Tuple[int, int, int]]], Union[None, Tuple[int, int, int]]]:
    for axis in range(3):
        for sign in [1, -1]:
            for axis_2 in {0, 1, 2} - {axis}:
                for sign_2 in [1, -1]:
                    unaligned = [reorient(reading, axis, sign, axis_2, sign_2) for reading in readings]
                    aligned, scanner_position = align(beacons, unaligned)
                    if aligned:
                        return aligned, scanner_position
    return None, None


def reorient(reading: Tuple[int, int, int], axis: int, sign: int, axis_2: int, sign_2: int) -> Tuple[int, int, int]:
    axis_3 = 3 - axis - axis_2
    sign_3 = 1 if (((axis_2 - axis) % 3 == 1) ^ (sign != sign_2)) else -1
    return reading[axis] * sign, reading[axis_2] * sign_2, reading[axis_3] * sign_3


def align(beacons: Set[Tuple[int, int, int]], unaligned: List[Tuple[int, int, int]]) -> \
        Tuple[Union[None, Set[Tuple[int, int, int]]], Union[None, Tuple[int, int, int]]]:
    for axis in range(3):
        known = sorted(beacons, key=lambda pos: pos[axis])
        unaligned.sort(key=lambda pos: pos[axis])
        known_diffs = diffs(known)
        unaligned_diffs = diffs(unaligned)
        inter = set(known_diffs) & set(unaligned_diffs)
        if inter:
            diff = inter.pop()
            kx, ky, kz = known[known_diffs.index(diff)]
            ux, uy, uz = unaligned[unaligned_diffs.index(diff)]
            ox, oy, oz = (ux - kx, uy - ky, uz - kz)
            moved = {(x - ox, y - oy, z - oz) for (x, y, z) in unaligned}
            matches = beacons & moved
            if len(matches) >= 12:
                return moved, (ox, oy, oz)
    return None, None


def diffs(positions: List[Tuple[int, int, int]]) -> List[Tuple[int, int, int]]:
    return [
        (x1 - x, y1 - y, z1 - z)
        for (x, y, z), (x1, y1, z1)
        in zip(positions, positions[1:])]


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        beacons, _ = self.orient_all_beacons()
        return len(beacons)

    def orient_all_beacons(self):
        scanner_readings = []
        for line in self.puzzle_input:
            if line.startswith('---'):
                scanner_readings.append([])
            elif line != '':
                scanner_readings[-1].append(tuple(map(int, line.split(','))))
        scanners = [(0, 0, 0)]
        beacons = set(scanner_readings[0])
        non_matched = scanner_readings[1:]
        reorient_all(beacons, scanners, non_matched)
        return beacons, scanners

    def process_input_part2(self) -> int:
        max_dist = 0
        _, scanners = self.orient_all_beacons()
        for x, y, z in scanners:
            for x1, y1, z1 in scanners:
                dist = abs(x1 - x) + abs(y1 - y) + abs(z1 - z)
                max_dist = max(dist, max_dist)
        return max_dist
