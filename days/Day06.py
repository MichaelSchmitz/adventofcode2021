from functools import lru_cache
from .AbstractDay import AbstractDay


@lru_cache(maxsize=128)
def calculate_spawned_fish(initial_ttl: int, days_to_go: int) -> int:
    fish = 1
    spawned = int((days_to_go + (6 - initial_ttl)) / 7)
    if 0 < days_to_go - initial_ttl <= 6:
        return 2
    for i in range(spawned):
        fish += calculate_spawned_fish(8, days_to_go - 1 - initial_ttl - i * 7)
    return fish


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        return self.calc_fish(80)

    def calc_fish(self, days: int):
        initial_state = [int(num) for num in self.puzzle_input[0].split(',')]
        total_fish = 0
        for fish in initial_state:
            total_fish += calculate_spawned_fish(fish, days)
        return total_fish

    def process_input_part2(self) -> int:
        return self.calc_fish(256)
