from __future__ import annotations
from .AbstractDay import AbstractDay


def _dimension_overlapping(a1: int, a2: int, o1: int, o2: int) -> bool:
    return a1 <= o2 and a2 >= o1


class Cube:
    def __init__(self, x1: int, x2: int, y1: int, y2: int, z1: int, z2: int) -> None:
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.z1 = z1
        self.z2 = z2

    def is_overlapping(self, cube: Cube) -> bool:
        return _dimension_overlapping(self.x1, self.x2, cube.x1, cube.x2) \
               and _dimension_overlapping(self.y1, self.y2, cube.y1, cube.y2) \
               and _dimension_overlapping(self.z1, self.z2, cube.z1, cube.z2)

    def get_intersection(self, cube: Cube) -> Cube:
        x1 = max(self.x1, cube.x1)
        x2 = min(self.x2, cube.x2)
        y1 = max(self.y1, cube.y1)
        y2 = min(self.y2, cube.y2)
        z1 = max(self.z1, cube.z1)
        z2 = min(self.z2, cube.z2)
        return Cube(x1, x2, y1, y2, z1, z2)

    def count(self) -> int:
        return (self.x2 - self.x1 + 1) * (self.y2 - self.y1 + 1) * (self.z2 - self.z1 + 1)


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        on = set()
        for row in self.puzzle_input:
            toggle_on = row.startswith('on')
            coords = row.split(' ')[1]
            _x, _y, _z = [coord[2:] for coord in coords.split(',')]
            x1, x2 = _x.split('..')
            x1 = int(x1)
            x2 = int(x2)
            y1, y2 = _y.split('..')
            y1 = int(y1)
            y2 = int(y2)
            z1, z2 = _z.split('..')
            z1 = int(z1)
            z2 = int(z2)
            new_state = set()
            if (x1 < -50 and x2 < -50) or (x1 > 50 and x2 > 50) \
                    or (y1 < -50 and y2 < -50) or (y1 > 50 and y2 > 50) \
                    or (z1 < -50 and z2 < -50) or (z1 > 50 and z2 > 50):
                continue
            for x in range(x1, x2 + 1):
                for y in range(y1, y2 + 1):
                    for z in range(z1, z2 + 1):
                        if -50 <= x <= 50 and -50 <= y <= 50 and -50 <= z <= 50:
                            new_state.add((x, y, z))
            if toggle_on:
                on |= new_state
            else:
                on -= new_state
        return len(on)

    def process_input_part2(self) -> int:
        on_cubes = []
        off_cubes = []
        for row in self.puzzle_input:
            toggle_on = row.startswith('on')
            coords = row.split(' ')[1]
            _x, _y, _z = [coord[2:] for coord in coords.split(',')]
            x1, x2 = _x.split('..')
            y1, y2 = _y.split('..')
            z1, z2 = _z.split('..')
            cube = Cube(int(x1), int(x2), int(y1), int(y2), int(z1), int(z2))
            overlapping = []
            off_toggled_on = []
            for _cube in on_cubes:
                if cube.is_overlapping(_cube):
                    overlapping.append(cube.get_intersection(_cube))
            for _cube in off_cubes:
                if cube.is_overlapping(_cube):
                    off_toggled_on.append(cube.get_intersection(_cube))
            on_cubes += off_toggled_on
            off_cubes += overlapping
            if toggle_on:
                on_cubes.append(cube)
        total = 0
        for cube in on_cubes:
            total += cube.count()
        for cube in off_cubes:
            total -= cube.count()
        return total
