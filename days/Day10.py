from typing import Dict

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    syntax_error_table: Dict[str, int] = {')': 3, ']': 57, '}': 1197, '>': 25137}
    completion_table: Dict[str, int] = {')': 1, ']': 2, '}': 3, '>': 4}
    inverted: Dict[str, str] = {')': '(', '}': '{', ']': '[', '>': '<', '(': ')', '{': '}', '[': ']', '<': '>'}

    def process_input_part1(self) -> int:
        syntax_errors = []
        for line in self.puzzle_input:
            char = self.get_corrupt_char(line)
            if char != '':
                syntax_errors.append(self.syntax_error_table[char])
        return sum(syntax_errors)

    def process_input_part2(self) -> int:
        scores = []
        for line in self.puzzle_input:
            score = 0
            char = self.get_corrupt_char(line)
            if char != '':
                continue
            opens = []
            for char in line:
                if char in ['(', '{', '[', '<']:
                    opens.append(char)
                elif self.is_closing(char, opens[-1]):
                    opens.pop(-1)
                else:
                    while not self.is_closing(char, opens[-1]):
                        score = score * 5 + self.completion_table[self.inverted[opens[-1]]]
                        opens.pop(-1)
            for char in reversed(opens):
                score = score * 5 + self.completion_table[self.inverted[char]]
            scores.append(score)
        return sorted(scores)[len(scores) // 2]

    def is_closing(self, char: str, last_char: str) -> bool:
        return char in [')', '}', ']', '>'] and self.inverted[char] == last_char

    def get_corrupt_char(self, line: str) -> str:
        opens = []
        for char in line:
            if char in ['(', '{', '[', '<']:
                opens.append(char)
            elif self.is_closing(char, opens[-1]):
                opens.pop(-1)
            else:
                return char
        return ''

