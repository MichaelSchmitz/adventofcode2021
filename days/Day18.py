from __future__ import annotations
import math
from ast import literal_eval
from itertools import permutations
from typing import List, Union

from .AbstractDay import AbstractDay


class Node:
    @classmethod
    def from_list(cls, numbers: List) -> Node:
        if isinstance(numbers, int):
            return Node(numbers)
        else:
            return Node(left=Node.from_list(numbers[0]), right=Node.from_list(numbers[1]))

    def __init__(self, value: Union[None, int] = None, left: Union[None, Node] = None, right: Union[None, Node] = None) -> None:
        self.value, self.left, self.right = value, left, right

    def flatted(self, flatted_list: Union[List[Node], None] = None) -> List[Node]:
        if flatted_list is None:
            flatted_list = []
        if self.value is not None:
            flatted_list.append(self)
        if self.left:
            self.left.flatted(flatted_list)
        if self.right:
            self.right.flatted(flatted_list)
        return flatted_list

    def replace(self, node: Node, to_replace: Node) -> None:
        if self.left == node:
            self.left = to_replace
        else:
            self.right = to_replace

    def magnitude(self):
        if self.value is not None:
            return self.value
        return 3 * self.left.magnitude() + 2 * self.right.magnitude()


def explode(node: Node, list_of_nodes: List[Node], depth: int = 0, parent: Node = None) -> bool:
    if node.value is not None:
        return False
    elif depth == 4:
        index = list_of_nodes.index(node.left)
        if index > 0:
            list_of_nodes[index - 1].value += node.left.value
        if index < len(list_of_nodes) - 2:
            list_of_nodes[index + 2].value += node.right.value
        parent.replace(node, Node(0))
        return True
    else:
        return explode(node.left, list_of_nodes, depth + 1, node) or explode(node.right, list_of_nodes, depth + 1, node)


def split(node: Node, depth: int = 0, parent: Node = None) -> bool:
    if node.value is not None:
        if node.value < 10:
            return False
        parent.replace(node, Node(left=Node(node.value // 2), right=Node(math.ceil(node.value / 2))))
        return True
    return split(node.left, depth + 1, node) or split(node.right, depth + 1, node)


def add(a: Node, b: Node) -> Node:
    temp = Node(left=a, right=b)
    performed = True
    while performed:
        if not explode(temp, temp.flatted()):
            if not split(temp):
                performed = False
    return temp


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        input_numbers = [literal_eval(line) for line in self.puzzle_input]
        result = Node.from_list(input_numbers[0])
        for input_number in input_numbers[1:]:
            to_add = Node.from_list(input_number)
            result = add(result, to_add)
        return result.magnitude()



    def process_input_part2(self) -> int:
        input_numbers = [literal_eval(line) for line in self.puzzle_input]
        return max([(add(Node.from_list(results[0]), Node.from_list(results[1]))).magnitude() for results in permutations(input_numbers, 2)])
