from typing import List, Dict
import heapq

from .AbstractDay import AbstractDay


class CoordPair:
    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def __eq__(self, other) -> bool:
        if isinstance(other, CoordPair):
            return self.x == other.x and self.y == other.y
        return False

    def __hash__(self):
        return hash((self.x, self.y))

    def __lt__(self, other):
        return (self.x, self.y) < (other.x, other.y)


def get_neighbours(base: CoordPair, target: CoordPair) -> List[CoordPair]:
    possibilities = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    neighbours = []
    for possibility in possibilities:
        new_x = base.x + possibility[0]
        new_y = base.y + possibility[1]
        if 0 <= new_x <= target.x and 0 <= new_y <= target.y:
            neighbours.append(CoordPair(new_x, new_y))
    return neighbours


def dijsktra(chitons: Dict[CoordPair, int], start: CoordPair, target: CoordPair, factor: int) -> int:
    shortest_path: Dict[CoordPair, int] = {start: 0}
    next_targets = [(0, start)]
    heapq.heapify(next_targets)
    while next_targets:
        _, pos = heapq.heappop(next_targets)

        if pos == target:
            break

        for neighbour in get_neighbours(pos, target):
            weight_to_node = shortest_path[pos]
            weight = chitons[CoordPair(neighbour.x % ((target.x + 1) // factor),
                                       neighbour.y % ((target.y + 1) // factor))]
            weight += neighbour.x // ((target.x + 1) // factor) + neighbour.y // ((target.y + 1) // factor)
            if weight > 9:
                weight %= 9
            total_weight = weight + weight_to_node

            if neighbour in shortest_path and total_weight < shortest_path[neighbour] or neighbour not in shortest_path:
                shortest_path[neighbour] = total_weight
                risk_level = total_weight
                heapq.heappush(next_targets, (risk_level, neighbour))
    return shortest_path[target]


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        chitons, count_x, count_y = self.parse_grid()
        return dijsktra(chitons, CoordPair(0, 0), CoordPair(count_x - 1, count_y - 1), 1)

    def process_input_part2(self) -> int:
        chitons, count_x, count_y = self.parse_grid()
        return dijsktra(chitons, CoordPair(0, 0), CoordPair(count_x * 5 - 1, count_y * 5 - 1), 5)

    def parse_grid(self):
        chitons = {}
        for y, row in enumerate(self.puzzle_input):
            for x, risk_level in enumerate(row):
                chitons[CoordPair(x, y)] = int(risk_level)
        count_x = len(self.puzzle_input[0])
        count_y = len(self.puzzle_input)
        return chitons, count_x, count_y
