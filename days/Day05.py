from __future__ import annotations
from typing import List, Dict

from .AbstractDay import AbstractDay


class Coord:
    @classmethod
    def from_string(cls: Coord, coord: str) -> Coord:
        x, y = coord.split(',')
        return Coord(x=x, y=y)

    def __init__(self, x: int, y: int) -> None:
        self.x = int(x)
        self.y = int(y)

    def __eq__(self, other) -> bool:
        if isinstance(other, Coord):
            return self.x == other.x and self.y == other.y
        return False

    def __hash__(self):
        return hash((self.x, self.y))


class CoordPair:
    def __init__(self, line: str) -> None:
        start, end = line.split(' -> ')
        self._start = Coord.from_string(coord=start)
        self._end = Coord.from_string(coord=end)

    def is_diagonal(self) -> bool:
        return self.is_vertical() and self.is_horizontal()

    def is_horizontal(self) -> bool:
        return self._start.x != self._end.x

    def is_vertical(self) -> bool:
        return self._start.y != self._end.y

    def get_positions(self) -> List[Coord]:
        ret = self.get_positions_without_diagonal()
        if len(ret) == 0:
            if self._start.x > self._end.x and self._start.y < self._end.y:
                ret = [Coord(x=x, y=y) for x, y in
                       zip(range(self._end.x, self._start.x + 1),
                           range(self._end.y, self._start.y - 1, -1))]
            elif self._start.x > self._end.x and self._start.y > self._end.y:
                ret = [Coord(x=x, y=y) for x, y in
                       zip(range(self._end.x, self._start.x + 1),
                           range(self._end.y, self._start.y + 1))]
            elif self._start.x < self._end.x and self._start.y < self._end.y:
                ret = [Coord(x=x, y=y) for x, y in
                       zip(range(self._start.x, self._end.x + 1),
                           range(self._start.y, self._end.y + 1))]
            else:  # _start.x < _end.x and _start.y > _end.y
                ret = [Coord(x=x, y=y) for x, y in
                       zip(range(self._start.x, self._end.x + 1),
                           range(self._start.y, self._end.y - 1, -1))]
        return ret

    def get_positions_without_diagonal(self) -> List[Coord]:
        if self.is_diagonal():
            return []
        elif self.is_horizontal():
            return [Coord(x=x, y=self._start.y) for x in
                    range(min(self._start.x, self._end.x), max(self._start.x, self._end.x) + 1)]
        else:  # vertical
            return [Coord(x=self._start.x, y=y) for y in
                    range(min(self._start.y, self._end.y), max(self._start.y, self._end.y) + 1)]


def count_overlap(positions: Dict[Coord, int]):
    count = 0
    for value in positions.values():
        if value > 1:
            count += 1
    return count


def add_to_positions(new_positions: List[Coord], positions: Dict[Coord, int]) -> None:
    for position in new_positions:
        if position in positions:
            positions[position] += 1
        else:
            positions[position] = 1


def print_lines_sample(positions: Dict[Coord, int]):
    for y in range(0, 10):
        output = ''
        for x in range(0, 10):
            if Coord(x, y) in positions:
                output += str(positions[Coord(x, y)])
            else:
                output += '.'
        print(output)


class Day(AbstractDay):
    def read_coords(self) -> List[CoordPair]:
        pairs = []
        for line in self.puzzle_input:
            pairs.append(CoordPair(line))
        return pairs

    def process_input_part1(self) -> int:
        pairs = self.read_coords()
        positions: Dict[Coord, int] = {}
        for pair in pairs:
            new_positions = pair.get_positions_without_diagonal()
            add_to_positions(new_positions, positions)
        # print_lines(positions)
        return count_overlap(positions)

    def process_input_part2(self) -> int:
        pairs = self.read_coords()
        positions: Dict[Coord, int] = {}
        for pair in pairs:
            new_positions = pair.get_positions()
            add_to_positions(new_positions, positions)
        # print_lines(positions)
        return count_overlap(positions)
