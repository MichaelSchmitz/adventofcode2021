from typing import List, Tuple

from .AbstractDay import AbstractDay


def count_bits(bits: List[List[int]], pos: int) -> Tuple[int, int]:
    zero_bit = 0
    one_bit = 0
    for i in range(len(bits)):
        if int(bits[i][pos]) == 0:
            zero_bit += 1
        else:
            one_bit += 1
    return zero_bit, one_bit


def filter_least_common(bits: List[List[int]], pos: int, ) -> List[List[int]]:
    zero_bit, one_bit = count_bits(bits, pos)
    least_common = 1 if one_bit < zero_bit else 0
    return [_bits for _bits in bits if int(_bits[pos]) == least_common]


def filter_most_common(bits: List[List[int]], pos: int, ) -> List[List[int]]:
    zero_bit, one_bit = count_bits(bits, pos)
    most_common = 0 if zero_bit > one_bit else 1
    return [_bits for _bits in bits if int(_bits[pos]) == most_common]


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        bits = list(map(list, self.puzzle_input))

        gamma_rate = ''
        epsilon_rate = ''
        for pos in range(len(bits[0])):
            zero_bit, one_bit = count_bits(bits, pos)
            if zero_bit < one_bit:
                gamma_rate += '1'
                epsilon_rate += '0'
            if zero_bit > one_bit:
                gamma_rate += '0'
                epsilon_rate += '1'
        return int(gamma_rate, 2) * int(epsilon_rate, 2)

    def process_input_part2(self) -> int:
        bits_co2 = list(map(list, self.puzzle_input))
        bits_o2 = bits_co2.copy()
        len_bits = len(bits_co2[0])
        pos = 0
        while len(bits_o2) > 1 and len_bits > pos:
            bits_o2 = filter_most_common(bits_o2, pos)
            pos += 1

        pos = 0
        while len(bits_co2) > 1 and len_bits > pos:
            bits_co2 = filter_least_common(bits_co2, pos)
            pos += 1
        o2 = int(''.join([str(x) for x in bits_o2[0]]), 2)
        co2 = int(''.join([str(x) for x in bits_co2[0]]), 2)
        return o2 * co2

