from typing import Tuple

from .AbstractDay import AbstractDay


def perform_step(x: int, y: int, x_vel: int, y_vel: int) -> Tuple[int, int, int, int]:
    x += x_vel
    y += y_vel
    if x_vel > 0:
        x_vel -= 1
    elif x_vel < 0:
        x_vel += 1
    return x, y, x_vel, y_vel - 1


def is_in_target_area(min_x: int, max_x: int, min_y: int, max_y: int, x: int, y: int) -> bool:
    return min_x <= x <= max_x and min_y <= y <= max_y


def will_be_in_target_area(min_x: int, max_x: int, min_y: int, max_y: int, vel_x: int, vel_y: int) -> Tuple[bool, int]:
    x = 0
    y = 0
    highest_y = 0
    while x < max_x + 1 and \
            not (vel_x == 0 and x < min_x) and \
            not (x > min_x and y < min_y):
        x, y, vel_x, vel_y = perform_step(x, y, vel_x, vel_y)
        highest_y = y if y > highest_y else highest_y
        if is_in_target_area(min_x, max_x, min_y, max_y, x, y):
            return True, highest_y
    return False, 0


class Day(AbstractDay):
    valid = 0

    def process_input_part1(self) -> int:
        _, _, x, y = self.puzzle_input[0].split(' ')
        x1, x2 = x[2:-1].split('..')
        y1, y2 = y[2:].split('..')
        min_x = min(int(x1), int(x2))
        max_x = max(int(x1), int(x2))
        min_y = min(int(y1), int(y2))
        max_y = max(int(y1), int(y2))
        highest = 0
        for vel_x in range(1, max_x * 2):
            for vel_y in range(min_y, max_x):
                hit, highest_y = will_be_in_target_area(min_x, max_x, min_y, max_y, vel_x, vel_y)
                if hit:
                    highest = highest_y if highest < highest_y else highest
                    self.valid += 1
        return highest

    def process_input_part2(self) -> int:
        return self.valid
