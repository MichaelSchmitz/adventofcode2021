from enum import Enum
from typing import Dict, Tuple

from .AbstractDay import AbstractDay


class SeaCucumber(Enum):
    East = '>'
    South = 'v'
    Empty = '.'


def right(coords: Tuple[int, int]) -> Tuple[int, int]:
    return coords[0] + 1, coords[1]


def right_wrap(coords: Tuple[int, int]) -> Tuple[int, int]:
    return 0, coords[1]


def down(coords: Tuple[int, int]) -> Tuple[int, int]:
    return coords[0], coords[1] + 1


def down_wrap(coords: Tuple[int, int]) -> Tuple[int, int]:
    return coords[0], 0


def move(sea_cucumber_map: Dict[Tuple[int, int], SeaCucumber], size_x: int, size_y: int) -> Dict[Tuple[int, int], SeaCucumber] | None:
    new_map = {}
    res = False
    for y in range(size_y):
        for x in range(size_x):
            coords = (x, y)
            entry = sea_cucumber_map[coords]
            if entry == SeaCucumber.East:
                right_coords = right(coords)
                if right_coords not in sea_cucumber_map.keys():
                    right_coords = right_wrap(coords)
                if sea_cucumber_map[right_coords] == SeaCucumber.Empty:
                    new_map[right_coords] = entry
                    res = True
                else:
                    new_map[coords] = entry

    for x in range(size_x):
        for y in range(size_y):
            coords = (x, y)
            if coords not in new_map.keys():
                new_map[coords] = SeaCucumber.Empty
    for x in range(size_x):
        for y in range(size_y):
            coords = (x, y)
            entry = sea_cucumber_map[coords]
            if entry == SeaCucumber.South:
                down_coords = down(coords)
                if down_coords not in sea_cucumber_map.keys():
                    down_coords = down_wrap(coords)
                if new_map[down_coords] == SeaCucumber.Empty and sea_cucumber_map[down_coords] != SeaCucumber.South:
                    new_map[down_coords] = entry
                    res = True
                else:
                    new_map[coords] = entry


    return new_map if res else None


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        sea_cucumber_map = {}
        for y, row in enumerate(self.puzzle_input):
            for x, pos in enumerate(row):
                sea_cucumber_map[(x, y)] = SeaCucumber(pos)
        steps = 0
        while sea_cucumber_map:
            sea_cucumber_map = move(sea_cucumber_map, len(self.puzzle_input[0]), len(self.puzzle_input))
            steps += 1
        return steps



    def process_input_part2(self) -> int:
        pass
