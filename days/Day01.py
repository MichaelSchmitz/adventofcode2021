from .AbstractDay import AbstractDay
from typing import List


def count_increases(measurements: List[int]) -> int:
    last_depth = int(measurements[0])
    counter = 0
    for depth in measurements:
        new_depth = int(depth)
        if last_depth < new_depth:
            counter += 1
        last_depth = new_depth
    return counter


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        return count_increases(list(map(int, self.puzzle_input)))

    def process_input_part2(self) -> int:
        windows = [self.puzzle_input[x:x+3] for x in range(0, len(self.puzzle_input))]
        reduced_windows = []
        for window in windows:
            reduced_windows.append(sum(list(map(int, window))))
        return count_increases(reduced_windows)
