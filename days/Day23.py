from collections import deque
from typing import Tuple

from .AbstractDay import AbstractDay

movement_costs = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}
target_room_base = {'A': 11, 'B': 13, 'C': 15, 'D': 17, '.': -1}
cant_stop = [2, 4, 6, 8]

target_room_base_two = {'A': 11, 'B': 15, 'C': 19, 'D': 23, '.': -1}
target_room_from_base = {11: 'A', 15: 'B', 19: 'C', 23: 'D'}


def create_new_state(state: str, switch_1: int, switch_2: int) -> str:
    tmp = list(state)
    tmp[switch_1], tmp[switch_2] = tmp[switch_2], tmp[switch_1]
    return ''.join(tmp)


def calc_moves(state: str) -> Tuple[str, int]:
    for room_index in [11, 13, 15, 17]:
        # target room not bottom
        if state[room_index + 1] == '.' and target_room_base[state[room_index]] == room_index:
            yield create_new_state(state, room_index, room_index + 1), movement_costs[state[room_index]]
        # bottom not target room
        if state[room_index] == '.' and target_room_base[state[room_index + 1]] != room_index and \
                target_room_base[state[room_index + 1]] != -1:
            yield create_new_state(state, room_index, room_index + 1), movement_costs[state[room_index + 1]]
        # non target room to hallway
        if state[room_index] != '.' and (target_room_base[state[room_index]] != room_index
                                         or state[room_index + 1] not in ['.', state[room_index]]):
            hallway_room_a = room_index - 9
            yield from move_into_hallway(hallway_room_a, room_index, state)
    # hallway movement to target room if possible
    for hallway in range(11):
        target = target_room_base[state[hallway]]
        if target != -1 and state[target] == '.' and state[target + 1] in ['.', state[hallway]]:
            hallway_room_a = target - 9
            yield from hallway_move(hallway, hallway_room_a, state, target)


def hallway_move(hallway, hallway_room_a, state, target):
    if hallway_room_a > hallway and \
            all(state[hallway_index] == '.' for hallway_index in range(hallway + 1, hallway_room_a)):
        yield create_new_state(state, hallway, target), \
              movement_costs[state[hallway]] * (hallway_room_a - hallway + 1)
    elif hallway_room_a < hallway and \
            all(state[hallway_index] == '.' for hallway_index in range(hallway_room_a, hallway)):
        yield create_new_state(state, hallway, target), \
              movement_costs[state[hallway]] * (hallway - hallway_room_a + 1)


def calc_moves_two(state: str) -> Tuple[str, int]:
    for room_index in [11, 15, 19, 23]:
        # target room not bottom
        if target_room_base_two[state[room_index]] == room_index \
                and all(state[room_index + i] in ['.', state[room_index]] for i in [2, 3]) \
                and state[room_index + 1] == '.':
            if state[room_index + 2] == '.':
                if state[room_index + 3] == '.':
                    yield create_new_state(state, room_index, room_index + 3), movement_costs[state[room_index]] * 3
                else:
                    yield create_new_state(state, room_index, room_index + 2), movement_costs[state[room_index]] * 2
            else:
                yield create_new_state(state, room_index, room_index + 1), movement_costs[state[room_index]]
        # bottom not target room
        if state[room_index] == '.' and any(
                state[room_index + i] not in ['.', target_room_from_base[room_index]] for i in range(1, 4)):
            if state[room_index + 1] != '.':
                yield create_new_state(state, room_index, room_index + 1), movement_costs[state[room_index + 1]]
            elif state[room_index + 2] != '.':
                yield create_new_state(state, room_index, room_index + 2), movement_costs[state[room_index + 2]] * 2
            else:
                yield create_new_state(state, room_index, room_index + 3), movement_costs[state[room_index + 3]] * 3
        # non target room to hallway
        if state[room_index] != '.' and (target_room_base_two[state[room_index]] != room_index
                                         or any(
                    state[room_index + i] not in ['.', state[room_index]] for i in range(1, 4))):
            hallway_room_a = (room_index - 11) // 2 + 2
            yield from move_into_hallway(hallway_room_a, room_index, state)
    # hallway movement to target room if possible
    for hallway in range(11):
        target = target_room_base_two[state[hallway]]
        if target != -1 and state[target] == '.' and all(
                state[target + i] in ['.', state[hallway]] for i in range(1, 4)):
            hallway_room_a = (target - 11) // 2 + 2
            yield from hallway_move(hallway, hallway_room_a, state, target)


def move_into_hallway(hallway_room_a, room_index, state):
    for hallway in range(hallway_room_a - 1, -1, -1):  # left
        if hallway not in cant_stop:
            if state[hallway] != '.':
                break
            yield create_new_state(state, room_index, hallway), \
                movement_costs[state[room_index]] * (hallway_room_a - hallway + 1)
    for hallway in range(hallway_room_a + 1, 11):  # right
        if hallway not in cant_stop:
            if state[hallway] != '.':
                break
            yield create_new_state(state, room_index, hallway), \
                movement_costs[state[room_index]] * (hallway - hallway_room_a + 1)


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        initial_state = '.' * 11
        a1, b1, c1, d1 = self.puzzle_input[2][3:-3].split('#')
        a2, b2, c2, d2 = self.puzzle_input[3][3:-1].split('#')
        initial_state += a1 + a2 + b1 + b2 + c1 + c2 + d1 + d2
        states = {initial_state: (0, None)}
        queue = deque([initial_state])
        while queue:
            state = queue.popleft()
            cost, previous = states[state]
            for next_move, next_cost in calc_moves(state):
                if next_move not in states.keys() or states[next_move][0] > cost + next_cost:
                    states[next_move] = (cost + next_cost, state)
                    queue.append(next_move)
        target_state = '...........AABBCCDD'
        path = []
        while target_state:
            path.append((target_state, states[target_state][0]))
            target_state = states[target_state][1]
        return states['...........AABBCCDD'][0]

    def process_input_part2(self) -> int:
        initial_state = '.' * 11
        a1, b1, c1, d1 = self.puzzle_input[2][3:-3].split('#')
        a2, b2, c2, d2 = self.puzzle_input[3][3:-1].split('#')
        initial_state += a1 + 'DD' + a2 + b1 + 'CB' + b2 + c1 + 'BA' + c2 + d1 + 'AC' + d2
        states = {initial_state: (0, None)}
        queue = deque([initial_state])
        while queue:
            state = queue.popleft()
            cost, previous = states[state]
            for next_move, next_cost in calc_moves_two(state):
                if next_move not in states.keys() or states[next_move][0] > cost + next_cost:
                    states[next_move] = (cost + next_cost, state)
                    queue.append(next_move)
        print(len(states))
        target_state = '...........AAAABBBBCCCCDDDD'
        path = []
        while target_state:
            path.append((target_state, states[target_state][0]))
            target_state = states[target_state][1]
        return states['...........AAAABBBBCCCCDDDD'][0]
