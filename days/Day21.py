from .AbstractDay import AbstractDay


class Dice:
    last = 100

    def roll(self):
        self.last = self.last % 100 + 1
        return self.last


class Track:
    def __init__(self, player1: int, player2: int) -> None:
        self.positions = [player1, player2]

    def move(self, player: int, steps: int) -> int:
        self.positions[player] = (self.positions[player] - 1 + steps) % 10 + 1
        return self.positions[player]


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        dice = Dice()
        player1, player2 = [int(row[-1]) for row in self.puzzle_input]
        track = Track(player1, player2)
        scores = [0, 0]
        dice_rolled = 0
        first = True
        while max(scores) < 1000:
            rolled = dice.roll() + dice.roll() + dice.roll()
            player = 0 if first else 1
            first = not first
            scores[player] += track.move(player, rolled)
            dice_rolled += 3
        return min(scores) * dice_rolled

    def process_input_part2(self) -> int:
        player1, player2 = [int(row[-1]) for row in self.puzzle_input]
        rolls = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
        universes = {(player1, player2, 0, 0, True): 1}
        wins = [0, 0]
        while universes:
            new_universes = {}
            for (player1, player2, score1, score2, first), count in universes.items():
                for rolled in rolls:
                    if first:
                        _player1 = (player1 + rolled) % 10
                        _score1 = score1 + (_player1 or 10)
                        if _score1 >= 21:
                            wins[0] += rolls[rolled] * count
                        else:
                            state = (_player1, player2, _score1, score2, False)
                            new_universes[state] = new_universes.get(state, 0) + rolls[rolled] * count
                    else:
                        _player2 = (player2 + rolled) % 10
                        _score2 = score2 + (_player2 or 10)
                        if _score2 >= 21:
                            wins[1] += rolls[rolled] * count
                        else:
                            state = (player1, _player2, score1, _score2, True)
                            new_universes[state] = new_universes.get(state, 0) + rolls[rolled] * count
            universes = new_universes
        return max(wins)


