from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        crab_subs = [int(num) for num in self.puzzle_input[0].split(',')]
        return min([sum([abs(crab_sub - i) for crab_sub in crab_subs]) for i in range(min(crab_subs), max(crab_subs))])

    def process_input_part2(self) -> int:
        crab_subs = [int(num) for num in self.puzzle_input[0].split(',')]
        return min([sum([int((abs(crab_sub - i) / 2) * (abs(crab_sub - i) + 1)) for crab_sub in crab_subs]) for i in
                    range(min(crab_subs), max(crab_subs))])
