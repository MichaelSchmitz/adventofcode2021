from typing import List

from .AbstractDay import AbstractDay


def remove_chars(chars: str, to_remove: str) -> str:
    out = ''
    for char in chars:
        if char not in to_remove:
            out += char
    return out


def common_chars(chars: str, second_chars: str) -> str:
    out = ''
    for char in chars:
        if char in second_chars:
            out += char
    return out


class SegmentConfig:
    digit = {
        'upper': '',
        'upperRight': '',
        'upperLeft': '',
        'middle': '',
        'lower': '',
        'lowerLeft': '',
        'lowerRight': ''
    }

    def _is_zero(self, to_check: str) -> bool:
        if len(to_check) == 6:
            return self.digit['upper'] in to_check and \
                   self.digit['upperRight'] in to_check and \
                   self.digit['upperLeft'] in to_check and \
                   self.digit['lowerRight'] in to_check and \
                   self.digit['lowerLeft'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def _is_one(self, to_check: str) -> bool:
        return len(to_check) == 2

    def _is_two(self, to_check: str) -> bool:
        if len(to_check) == 5:
            return self.digit['upper'] in to_check and \
                   self.digit['upperRight'] in to_check and \
                   self.digit['middle'] in to_check and \
                   self.digit['lowerLeft'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def _is_three(self, to_check: str) -> bool:
        if len(to_check) == 5:
            return self.digit['upper'] in to_check and \
                   self.digit['upperRight'] in to_check and \
                   self.digit['middle'] in to_check and \
                   self.digit['lowerRight'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def _is_four(self, to_check: str) -> bool:
        return len(to_check) == 4

    def _is_five(self, to_check: str) -> bool:
        if len(to_check) == 5:
            return self.digit['upper'] in to_check and \
                   self.digit['upperLeft'] in to_check and \
                   self.digit['middle'] in to_check and \
                   self.digit['lowerRight'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def _is_six(self, to_check: str) -> bool:
        if len(to_check) == 6:
            return self.digit['upper'] in to_check and \
                   self.digit['upperLeft'] in to_check and \
                   self.digit['middle'] in to_check and \
                   self.digit['lowerLeft'] in to_check and \
                   self.digit['lowerRight'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def _is_seven(self, to_check: str) -> bool:
        return len(to_check) == 3

    def _is_eight(self, to_check: str) -> bool:
        return len(to_check) == 7

    def _is_nine(self, to_check: str) -> bool:
        if len(to_check) == 6:
            return self.digit['upper'] in to_check and \
                   self.digit['upperLeft'] in to_check and \
                   self.digit['upperRight'] in to_check and \
                   self.digit['middle'] in to_check and \
                   self.digit['lowerRight'] in to_check and \
                   self.digit['lower'] in to_check
        return False

    def guess_config(self, inputs: List[str]) -> None:
        one, four, zero_six_or_nine, seven, eight = '', '', [], '', ''
        for number in inputs[:]:
            if len(number) == 2:
                one = number
            elif len(number) == 3:
                seven = number
            elif len(number) == 4:
                four = number
            elif len(number) == 6:
                zero_six_or_nine.append(number)
            elif len(number) == 7:
                eight = number
        zero, six, nine = '', '', ''
        for num in zero_six_or_nine:
            if len(remove_chars(num, one)) == 5:
                six = num
            elif len(remove_chars(num, four)) == 2:
                nine = num
            else:
                zero = num
        self.digit['upper'] = remove_chars(seven, one)
        self.digit['upperRight'] = remove_chars(eight, six)
        self.digit['lowerLeft'] = remove_chars(eight, nine)
        self.digit['middle'] = remove_chars(eight, zero)
        self.digit['lowerRight'] = remove_chars(one, self.digit['upperRight'])
        self.digit['lower'] = remove_chars(eight, four + self.digit['upper'] + self.digit['lowerLeft'])
        self.digit['upperLeft'] = remove_chars(zero,
                                               one + self.digit['upper'] + self.digit['middle'] + self.digit['lower'] +
                                               self.digit['lowerLeft'])

    def parse_output(self, output: List[str]) -> int:
        out_str = ''
        for digit in output:
            if self._is_zero(digit):
                out_str += '0'
            if self._is_one(digit):
                out_str += '1'
            elif self._is_two(digit):
                out_str += '2'
            elif self._is_three(digit):
                out_str += '3'
            elif self._is_four(digit):
                out_str += '4'
            elif self._is_five(digit):
                out_str += '5'
            elif self._is_six(digit):
                out_str += '6'
            elif self._is_seven(digit):
                out_str += '7'
            elif self._is_eight(digit):
                out_str += '8'
            elif self._is_nine(digit):
                out_str += '9'
        return int(out_str)


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        outputs = [outputs.split(' ') for outputs in [line.split(' | ')[1] for line in self.puzzle_input]]
        unique = 0
        for output in [output for sub in outputs for output in sub]:
            if len(output) in [2, 3, 4, 7]:
                unique += 1
        return unique

    def process_input_part2(self) -> int:
        out = 0
        for line in self.puzzle_input:
            config = SegmentConfig()
            config.guess_config(line.split(' | ')[0].split(' '))
            out += config.parse_output(line.split(' | ')[1].split(' '))
        return out
