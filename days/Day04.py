from typing import List, Tuple
import re

from .AbstractDay import AbstractDay


def filter_rows_and_columns(called_num: int, board: Tuple[List[List[int]], List[List[int]]]) -> bool:
    for row in board[0]:
        if called_num in row:
            row.remove(called_num)
            if len(row) == 0:
                return True
    for column in board[1]:
        if called_num in column:
            column.remove(called_num)
            if len(column) == 0:
                return True
    return False


def mark_till_win_last(calling_nums: List[int], boards: List[Tuple[List[List[int]], List[List[int]]]]) -> \
        Tuple[List[List[int]], int]:
    for called_num in calling_nums:
        for board in boards[:]:
            if filter_rows_and_columns(called_num, board) is True:
                if len(boards) > 1:
                    boards.remove(board)
                else:
                    return board[0], called_num


def mark_till_win(calling_nums: List[int], boards: List[Tuple[List[List[int]], List[List[int]]]]) -> \
        Tuple[List[List[int]], int]:
    for called_num in calling_nums:
        for board in boards:
            for row in board[0]:
                if called_num in row:
                    row.remove(called_num)
                    if len(row) == 0:
                        return board[0], called_num
            for column in board[1]:
                if called_num in column:
                    column.remove(called_num)
                    if len(column) == 0:
                        return board[1], called_num


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        calling_nums = [int(num) for num in self.puzzle_input[0].split(',')]
        boards = self.extract_boards()
        winning_board, called_num = mark_till_win(calling_nums, boards)
        unmarked_sum = sum(sum(row) for row in winning_board)
        return unmarked_sum * called_num

    def process_input_part2(self) -> int:
        calling_nums = [int(num) for num in self.puzzle_input[0].split(',')]
        boards = self.extract_boards()
        last_winning_board, called_num = mark_till_win_last(calling_nums, boards)
        unmarked_sum = sum(sum(row) for row in last_winning_board)
        return unmarked_sum * called_num

    def extract_boards(self) -> List[Tuple[List[List[int]], List[List[int]]]]:
        boards = []
        board_rows = [[] for _ in range(5)]
        board_columns = []
        for line in self.puzzle_input[2::]:
            column = []
            if line == '':
                boards.append((board_rows, board_columns))
                board_rows = [[] for _ in range(5)]
                board_columns = []
            else:
                # replace all starting and double white spaces to allow splitting per number
                for index, num in enumerate(re.sub('^ ', '', re.sub(' {2,}', ' ', line)).split(' ')):
                    column.append(int(num))
                    board_rows[index].append(int(num))
                board_columns.append(column)
        boards.append((board_rows, board_columns))
        return boards
