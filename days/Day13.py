from typing import Tuple, Set

from .AbstractDay import AbstractDay


def print_dots(dots: Set[Tuple[int, int]], max_x: int, max_y: int) -> None:
    for y in range(max_y + 1):
        row = ''
        for x in range(max_x + 1):
            if (x, y) in dots:
                row += '#'
            else:
                row += '.'
        print(row)


def perform_fold(dots, max_x, max_y, row) -> Tuple[int, int]:
    axis, index = row.split('=')
    fold_index = int(index)
    if axis[-1] == 'y':
        for x in range(max_x + 1):
            for y in range(fold_index + 1, max_y + 1):
                if (x, y) in dots:
                    dots.remove((x, y))
                    new_y = fold_index - (y - fold_index)
                    if (x, new_y) not in dots:
                        dots.add((x, new_y))
        max_y -= max_y - fold_index + 1
    elif axis[-1] == 'x':
        for x in range(fold_index + 1, max_x + 1):
            for y in range(max_y + 1):
                if (x, y) in dots:
                    dots.remove((x, y))
                    new_x = fold_index - (x - fold_index)
                    if (new_x, y) not in dots:
                        dots.add((new_x, y))
        max_x -= max_x - fold_index + 1
    return max_x, max_y


def add_dot(dots, max_x, max_y, row):
    _x, _y = row.split(',')
    x = int(_x)
    y = int(_y)
    if x > max_x:
        max_x = x
    if y > max_y:
        max_y = y
    dots.add((x, y))
    return max_x, max_y


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        reached_instructions = False
        dots: Set[Tuple[int, int]] = set()
        max_x = 0
        max_y = 0
        for row in self.puzzle_input:

            if row == '':
                reached_instructions = True
            elif not reached_instructions:
                max_x, max_y = add_dot(dots, max_x, max_y, row)
            else:
                max_x, max_y = perform_fold(dots, max_x, max_y, row)
                return len(dots)
        return -1

    def process_input_part2(self) -> int:
        reached_instructions = False
        dots: Set[Tuple[int, int]] = set()
        max_x = 0
        max_y = 0
        for row in self.puzzle_input:

            if row == '':
                reached_instructions = True
            elif not reached_instructions:
                max_x, max_y = add_dot(dots, max_x, max_y, row)
            else:
                max_x, max_y = perform_fold(dots, max_x, max_y, row)
        print_dots(dots, max_x, max_y)
        return None
