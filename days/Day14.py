from collections import Counter
from functools import cache

from .AbstractDay import AbstractDay


class Day(AbstractDay):
    insertion_rules = {}
    performed_replacements = {}

    def process_input_part1(self) -> int:
        polymer = self.read_template_instructions()
        for _ in range(10):
            polymer = self.perform_iteration(polymer)
        char_counts = Counter(polymer)
        return char_counts.most_common(1)[0][1] - char_counts.most_common()[-1][1]

    def read_template_instructions(self):
        template = self.puzzle_input[0]
        for line in self.puzzle_input[2:]:
            key, value = line.split(' -> ')
            self.insertion_rules[key] = value
        return template

    def process_input_part2(self) -> int:
        polymer = self.read_template_instructions()
        # return self.use_divide_and_conquer(polymer)
        return self.use_iteration_first(polymer)

    def use_iteration_first(self, polymer: str) -> int:
        counter = self.perform_iteration_first(polymer)
        return counter.most_common(1)[0][1] - counter.most_common()[-1][1]

    def use_divide_and_conquer(self, polymer: str) -> int:
        for _ in range(40):
            polymer = self.perform_iteration(polymer)
        char_counts = Counter(polymer)
        return char_counts.most_common(1)[0][1] - char_counts.most_common()[-1][1]

    def perform_naive_iteration(self, polymer):
        _polymer = polymer
        inserted = 0
        for index in range(0, len(polymer) - 1):
            key = polymer[index:index + 2]
            if key in self.insertion_rules.keys():
                inserted += 1
                _polymer = _polymer[:index + inserted] + self.insertion_rules[key] + _polymer[index + inserted:]
        polymer = _polymer
        return polymer

    def perform_iteration_first(self, polymer: str) -> Counter:
        @cache
        def perform_iteration_first_cached(polymer_part: str, iteration: int) -> Counter:
            nonlocal insertion_rules
            count = Counter()
            if iteration < 40:
                for i in range(1, len(polymer_part)):
                    val = insertion_rules[polymer_part[i-1:i+1]]
                    count.update(val)
                    new_part = polymer_part[i - 1] + val + polymer_part[i]
                    inner_count = perform_iteration_first_cached(new_part, iteration + 1)
                    count += inner_count
            return count
        insertion_rules = self.insertion_rules
        return Counter(polymer) + perform_iteration_first_cached(polymer, 0)

    def perform_iteration(self, polymer_part: str) -> str:
        if len(polymer_part) < 2:
            return polymer_part
        if len(polymer_part) == 2:
            if polymer_part in self.insertion_rules.keys():
                return polymer_part[:1] + self.insertion_rules[polymer_part] + polymer_part[1:]
            else:
                return polymer_part
        else:
            if polymer_part in self.performed_replacements.keys():
                return self.performed_replacements[polymer_part]
            polymer_part_a = polymer_part[:len(polymer_part) // 2 + 1]  # we need to overlap one char
            polymer_part_b = polymer_part[len(polymer_part) // 2:]
            polymer_part_a = self.perform_iteration(polymer_part_a)
            polymer_part_b = self.perform_iteration(polymer_part_b)[1:]  # avoid overlap
            self.performed_replacements[polymer_part] = polymer_part_a + polymer_part_b
            return polymer_part_a + polymer_part_b

