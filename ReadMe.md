# Advent of Code 2021

Puzzle input is to be provided in the subfolder "quizInput" in the form "XX.txt" where XX is the zero-padded day number.

To create a new day simply execute the given script `create_new_day.sh <DayNumber>`.
