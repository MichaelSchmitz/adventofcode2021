import unittest

from main import process_day
from common import InputSample


class TestDays(unittest.TestCase):
    def test_day(self):
        for day in [
            '01',
            '02',
            '03',
            '04',
            '05',
            '06',
            '07',
            '08',
            '09',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
            '24',
            '25',
        ]:
            self._day(day=day)

    def _day(self, day: str):
        expected_one, expected_two = InputSample.read_expected(day)
        out_one, out_two = process_day(day, InputSample.read_input(day))
        self.assertEqual(expected_one, out_one)
        if expected_two != '':
            self.assertEqual(expected_two, out_two)


if __name__ == '__main__':
    unittest.main()
