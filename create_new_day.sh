#!/bin/bash

read -r -d '' pyfile << EOF
from .AbstractDay import AbstractDay


class Day(AbstractDay):
    def process_input_part1(self) -> int:
        pass

    def process_input_part2(self) -> int:
        pass
EOF

echo "Provide the sample input and press ctrl-d when done:"
sample_input=$(cat)

echo "Provide the solution for the first sample and press ctrl-d when done:"
expected_sample=$(cat)

echo "Provide your puzzle input and press ctrl-d when done:"
puzzle_input=$(cat)

printf "%s\n" "$pyfile" > "days/Day$1.py"
printf "%s" "$sample_input" > "sampleInput/$1.txt"
printf "%s" "$expected_sample" > "sampleExpected/$1.txt"
printf "%s" "$puzzle_input" > "quizInput/$1.txt"

sed -i 's/           ]/           '\''Day'"$1"''\'',\n           ]/g' days/__init__.py
sed '$ {/^$/d;}' main.py
printf "    process_day('%s')\n" "$1" >> main.py
sed -i 's/            }/            '\'''"$1"''\'': Day'"$1"',\n            }/g' days/DayFactory.py
sed -i 's/        ]/            '\'"$1"\'',\n        ]/g' mainSample.py
