from typing import List


def read_input(name: str) -> List[str]:
    with open(f'quizInput/{name}.txt', 'r') as file:
        lines = file.readlines()
        return [line.rstrip() for line in lines]
