from typing import List, Tuple, Union


def read_input(name: str) -> List[str]:
    with open(f'sampleInput/{name}.txt', 'r') as file:
        lines = file.readlines()
        return [line.rstrip() for line in lines]


def read_expected(name: str) -> Union[Tuple[int, None], Tuple[int, int]]:
    with open(f'sampleExpected/{name}.txt', 'r') as file:
        result = file.readlines()[0:2]
        if len(result) == 1:
            return int(result[0]), None
        return int(result[0]), int(result[1])
