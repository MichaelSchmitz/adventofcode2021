def print_day_result(day: str, part: int, result: any) -> None:
    print(f'Day {day}, part {part}: {result}')
